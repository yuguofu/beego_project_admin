# beego_project_admin

#### 介绍
beego博客项目，此为前端后台管理项目，使用vue cli4.5和Ant Design of Vue

#### 软件架构
vue cli4.5 + Ant Design of Vue  

beego后端+前台项目 `beegoblog` 地址为： [https://gitee.com/yuguofu/beegoblog](https://gitee.com/yuguofu/beegoblog) ，配合使用。


#### 安装教程

1.  克隆到本地
2.  npm install安装依赖
3.  npm run serve调试  
4.  npm build打包

#### 预览  
![文章管理](https://images.gitee.com/uploads/images/2020/1213/222206_b99ede0c_5348320.png "屏幕截图.png")  
![用户管理](https://images.gitee.com/uploads/images/2020/1213/222245_c06bc052_5348320.png "屏幕截图.png")  
![写文章](https://images.gitee.com/uploads/images/2020/1213/222324_2a371540_5348320.png "屏幕截图.png")