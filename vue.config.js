// vue.config.js
module.exports = {
    // 部署应用包时的基本 URL
    publicPath: "/admin/",
    // 打包(构建)输出路径
    outputDir: "dist",
    // 静态资源目录
    assetsDir: "static"

}